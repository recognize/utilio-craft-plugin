<?php

use craft\test\TestSetup;

ini_set('date.timezone', 'UTC');
date_default_timezone_set('UTC');

// Use the current installation of Craft
define('CRAFT_STORAGE_PATH', __DIR__ . '/craft_folders/storage');
define('CRAFT_TEMPLATES_PATH', __DIR__ . '/craft_folders/templates');
define('CRAFT_CONFIG_PATH', __DIR__ . '/craft_folders/config');
define('CRAFT_MIGRATIONS_PATH', __DIR__ . '/craft_folders/migrations');
define('CRAFT_TRANSLATIONS_PATH', __DIR__ . '/craft_folders/translations');
define('CRAFT_VENDOR_PATH', dirname(__DIR__) . '/../vendor');

TestSetup::configureCraft();
