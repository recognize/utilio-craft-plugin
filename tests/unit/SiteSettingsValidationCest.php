<?php

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use recognize\utilio\models\Settings;
use recognize\utilio\models\SiteSettings;
use recognize\utilio\Utilio;

/**
 */
class SiteSettingsValidationCest
{
    /**
     * @param UnitTester $i
     */
    public function testSiteSettingsValidation(UnitTester $i): void
    {
        $this->initPlugin();

        $settings = new Settings(
            [
                'siteSettings' => [
                    'default' => new SiteSettings(
                        [
                            'endpoint' => 'http://utilio.test.recognize.hosting',
                            'apiKey'   => 'secret',
                        ]
                    ),
                ],
            ]
        );

        $settings->validate();
        $i->assertEmpty($settings->errors);

        $settings = new Settings(
            [
                'siteSettings' => [
                    'default' => new SiteSettings(
                        [
                            'endpoint' => 'http://invalid url',
                            'apiKey'   => 'secret',
                        ]
                    ),
                ],
            ]
        );

        $settings->validate();
        $i->assertNotEmpty($settings->errors);

        $settings = new Settings(
            [
                'siteSettings' => [
                    'default' => new SiteSettings(
                        [
                            'endpoint' => 'http://utilio.nl',
                            'apiKey'   => '',
                        ]
                    ),
                ],
            ]
        );

        $settings->validate();
        $i->assertNotEmpty($settings->errors);
    }

    /**
     * @return void
     */
    private function initPlugin() {
        (new Utilio('utilio'))->init();
        Utilio::$plugin->setComponents(
            [
                'client' => $this->createMockClient(),
            ]
        );
    }

    private function createMockClient(): Client
    {
        $mock = new MockHandler(
            [
                new Response(200),
                new Response(200),
                new Response(200)
            ]
        );

        $handler = HandlerStack::create($mock);

        return new Client(['handler' => $handler]);
    }
}
