<?php

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use recognize\utilio\events\CreateClientEvent;
use recognize\utilio\exceptions\UtilioException;
use recognize\utilio\models\SiteSettings;
use recognize\utilio\services\UtilioService;
use recognize\utilio\services\UtilioServiceInterface;
use yii\base\Event;

class UtilioServiceCest
{
    /**
     * @var Request[] Container with history of the Openshift client
     */
    private $container = [];

    /** @var callable */
    private $history;

    /**
     * @var ClientInterface
     */
    private $client;

    public function _before()
    {
        $this->container = [];
        $this->history = Middleware::history($this->container);

        Event::on(UtilioService::class, UtilioService::EVENT_CREATE_CLIENT, function(CreateClientEvent $event) {
            $event->client = $this->client;
        });
    }

    /**
     * @param UnitTester $i
     * @throws UtilioException
     */
    public function testGetProductExpectResponse(UnitTester $i): void
    {
        $this->setUpMockResponse([
            new Response(200, [], json_encode([
                'code' => 'AABB',
                'name' => 'Test',
                'brand' => [
                    'name' => 'new'
                ],
                'serviceRadius' => 0
            ])),
        ]);

        $product = $this->createUtilioService()->getProduct('AABB');

        $i->assertCount(1, $this->container);
        $firstRequest = $this->container[0]['request'];
        $i->assertEquals('/client-api/v1/products/AABB', (string) $firstRequest->getUri());

        $i->assertEquals('AABB', $product['code'] ?? '');
        $i->assertEquals('Test', $product['name'] ?? '');
    }

    /**
     * @param UnitTester $i
     */
    public function testGetProductExpectException(UnitTester $i): void
    {
        $this->setUpMockResponse([
            new RequestException(
                'Not found',
                new Request('GET', '/client-api/v1/products/AABB'),
                new Response(404)
            ),
        ]);

        $i->expectThrowable(UtilioException::class, function() {
            $this->createUtilioService()->getProduct('AABB');
        });
    }

    private function createUtilioService(): UtilioServiceInterface
    {
        $siteSettings = new SiteSettings();
        $siteSettings->endpoint = 'http://127.0.0.1';
        $siteSettings->apiKey = 'blablabla';

        $utilioService = new UtilioService([
            'siteSettings' => $siteSettings
        ]);

        return $utilioService;
    }

    /**
     * @param array $responses
     */
    private function setUpMockResponse(array $responses): void
    {
        $mock = new MockHandler($responses);

        $history = Middleware::history($this->container);

        $handler = HandlerStack::create($mock);
        $handler->push($history);

        $this->client = new Client(['handler' => $handler]);
    }
}
