#!/usr/bin/env sh
set -e

composer install

nginx &
php-fpm
