<?php

namespace recognize\utilio\validators;

use LogicException;
use recognize\utilio\models\Settings;
use recognize\utilio\models\SiteSettings;
use yii\base\Model;
use yii\validators\Validator;

/**
 * @package recognize\utilio\validators
 */
class SiteSettingsValidator extends Validator
{
    /**
     * @param Model  $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute): void
    {
        if (!$model instanceof Settings) {
            return;
        }

        if (!isset($model->siteSettings[$model->selectedSite])) {
            throw new LogicException('Selected site does not exist on siteSettings property');
        }

        $this->validateSelectedSiteSettings($model);
    }

    /**
     * @param Settings $model
     * @return void
     */
    private function validateSelectedSiteSettings(Settings $model): void
    {
        $siteSettings = new SiteSettings($model->siteSettings[$model->selectedSite]);

        if (!$siteSettings->validate()) {
            foreach ($siteSettings->errors as $attributeErrors) {
                foreach ($attributeErrors as $error) {
                    $this->addError($model, 'siteSettings', $error);
                }
            }
        }
    }
}
