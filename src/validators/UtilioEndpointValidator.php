<?php

namespace recognize\utilio\validators;

use GuzzleHttp\Exception\TransferException;
use Psr\Http\Message\ResponseInterface;
use recognize\utilio\Utilio;
use yii\base\Model;
use yii\validators\Validator;

/**
 * Class UtilioEndpointValidator
 * @package recognize\utilio\validators
 */
class UtilioEndpointValidator extends Validator
{
    /**
     * @param Model  $model
     * @param string $attribute
     */
    public function validateAttribute($model, $attribute): void
    {
        $endpoint = rtrim($model->$attribute, '/');
        $apiKey = property_exists($model, 'apiKey') ? $model->apiKey : '';
        $valid = false;

        try {
            $client = Utilio::$plugin->client;
            /** @var ResponseInterface $response */
            $response = $client->get($endpoint . '/client-api/v1/products', [
                'headers' => [
                    'Authorization' => 'apikey ' . $apiKey
                ],
                'query' => [
                    'page' => 1
                ]
            ]);

            $valid = $response->getStatusCode() === 200;
        } catch (TransferException $ex) {
            \Craft::error($ex);
        }

        if (!$valid) {
            $this->addError($model, $attribute, 'Invalid Utilio endpoint');
        }
    }
}
