<?php

namespace recognize\utilio\popos;

/**
 * @package recognize\utilio\popos
 */
class ProductRequest implements \JsonSerializable
{
    /**
     * @var string
     */
    private $_product;

    /**
     * @var int
     */
    private $_amount;

    /**
     * @param string $product
     * @param int $amount
     */
    public function __construct(string $product, int $amount = 1)
    {
        $this->_product = $product;
        $this->_amount = $amount;
    }

    /**
     * @return string
     */
    public function getProduct(): string
    {
        return $this->_product;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->_amount;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'product' => $this->_product,
            'amount' => $this->_amount,
        ];
    }
}
