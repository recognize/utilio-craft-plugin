<?php

namespace recognize\utilio\popos;

/**
 * Class Address
 * @package recognize\utilio\popos
 */
class Address implements \JsonSerializable
{
    /**
     * @var string
     */
    private $_street;

    /**
     * @var string
     */
    private $_streetNumber;

    /**
     * @var string
     */
    private $_postcode;

    /**
     * @var string
     */
    private $_city;

    /**
     * Address constructor.
     * @param string $streetNumber
     * @param string $postcode
     */
    public function __construct(string $streetNumber, string $postcode)
    {
        $this->_streetNumber = $streetNumber;
        $this->_postcode = $postcode;
    }

    /**
     * @return string
     */
    public function getStreetNumber(): string
    {
        return $this->_streetNumber;
    }

    /**
     * @return string
     */
    public function getPostcode(): string
    {
        return $this->_postcode;
    }

    /**
     * @return string
     */
    public function getStreet(): string
    {
        return $this->_street;
    }

    /**
     * @param string $_street
     * @return Address
     */
    public function setStreet(string $_street): Address
    {
        $this->_street = $_street;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->_city;
    }

    /**
     * @param string $_city
     * @return Address
     */
    public function setCity(string $_city): Address
    {
        $this->_city = $_city;
        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'streetNumber' => $this->_streetNumber,
            'postcode' => $this->_postcode,
        ];
    }
}
