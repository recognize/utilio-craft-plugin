<?php

namespace recognize\utilio\popos;

/**
 * Class AvailabilityRequest
 * @package recognize\utilio\popos
 */
class AvailabilityRequest implements \JsonSerializable
{
    public const DEFAULT_DATE_FORMAT = 'Y-m-d';

    /**
     * @var \DateTime
     */
    private $_from;

    /**
     * @var \DateTime
     */
    private $_until;

    /**
     * @var array
     */
    private $_products = [];

    /** @var Address|null */
    private $_address;

    /**
     * AvailabilityRequest constructor.
     * @param \DateTime $from
     * @param \DateTime $until
     */
    public function __construct(\DateTime $from, \DateTime $until)
    {
        $this->_from = $from;
        $this->_until = $until;
    }

    /**
     * @return \DateTime
     */
    public function getFrom(): \DateTime
    {
        return $this->_from;
    }

    /**
     * @return \DateTime
     */
    public function getUntil(): \DateTime
    {
        return $this->_until;
    }

    /**
     * @return array
     */
    public function getProducts(): array
    {
        return $this->_products;
    }

    /**
     * @param ProductRequest $productRequest
     * @return $this
     */
    public function addProduct(ProductRequest $productRequest): self
    {
        $this->_products[] = $productRequest;

        return $this;
    }

    /**
     * @return Address|null
     */
    public function getAddress(): ?Address
    {
        return $this->_address;
    }

    /**
     * @param Address|null $address
     */
    public function setAddress(?Address $address)
    {
        $this->_address = $address;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        $products = [];
        foreach ($this->_products as $product) {
            $products[] = $product->jsonSerialize();
        }

        $json = [
            'from' => $this->getFrom()->format(self::DEFAULT_DATE_FORMAT),
            'until' => $this->getUntil()->format(self::DEFAULT_DATE_FORMAT),
            'products' => $products,
        ];

        if ($this->_address !== null) {
            $json['address'] = $this->_address->jsonSerialize();
        }

        return $json;
    }
}
