<?php

namespace recognize\utilio\popos;

/**
 * Class Product
 * @package recognize\utilio\popos
 */
class Product implements \JsonSerializable
{
    /**
     * @var string
     */
    private $_code;

    /**
     * @var string
     */
    private $_description;

    /**
     * Product constructor.
     * @param string $code
     */
    public function __construct(string $code)
    {
        $this->_code = $code;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->_code;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->_description;
    }

    /**
     * @param string $description
     * @return self
     */
    public function setDescription(string $description): self
    {
        $this->_description = $description;

        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'code' => $this->_code,
        ];
    }
}
