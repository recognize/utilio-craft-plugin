<?php

namespace recognize\utilio\popos;

/**
 * Class Job
 * @package recognize\utilio\popos
 */
class Job implements \JsonSerializable
{
    public const DATE_FORMAT = 'Y-m-d';

    /**
     * @var string|null
     */
    private $_id;

    /**
     * @var string
     */
    private $_consumerName = '';

    /**
     * @var string
     */
    private $_consumerEmail = '';

    /**
     * @var string
     */
    private $_consumerPhoneNumber = '';

    /**
     * @var string|null
     */
    private $_consumerGender = '';
    
    /**
     * @var bool|null
     */
    private $_businessJob = false;

    /**
     * @var string|null
     */
    private $_vatNumber = '';
    
    /**
     * @var Address|null
     */
    private $_address;

    /**
     * @var \DateTime|null
     */
    private $_date;

    /**
     * @var bool
     */
    private $_morning = false;

    /**
     * @var bool
     */
    private $_afternoon = false;

    /**
     * @var bool
     */
    private $_evening = false;

    /**
     * @var Product
     */
    private $_primaryProduct;

    /**
     * @var array key as id, value as amount
     */
    private $_secondaryProducts;

    /**
     * @var string
     */
    private $_comments = '';

    /**
     * @var array
     */
    private $_attachments = [];
    
    /**
     * @return null|string
     */
    public function getId(): ?string
    {
        return $this->_id;
    }

    /**
     * @param null|string $_id
     * @return Job
     */
    public function setId(?string $_id): Job
    {
        $this->_id = $_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getConsumerName(): string
    {
        return $this->_consumerName;
    }

    /**
     * @param string $_consumerName
     * @return self
     */
    public function setConsumerName(string $_consumerName): self
    {
        $this->_consumerName = $_consumerName;

        return $this;
    }

    /**
     * @return string
     */
    public function getConsumerEmail(): string
    {
        return $this->_consumerEmail;
    }

    /**
     * @param string $_consumerEmail
     * @return self
     */
    public function setConsumerEmail(string $_consumerEmail): self
    {
        $this->_consumerEmail = $_consumerEmail;

        return $this;
    }

    /**
     * @return string
     */
    public function getConsumerPhoneNumber(): string
    {
        return $this->_consumerPhoneNumber;
    }

    /**
     * @param string $_consumerPhoneNumber
     * @return self
     */
    public function setConsumerPhoneNumber(string $_consumerPhoneNumber): self
    {
        $this->_consumerPhoneNumber = $_consumerPhoneNumber;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getConsumerGender(): ?string
    {
        return $this->_consumerGender;
    }

    /**
     * @param string|null $consumerGender
     * @return Job
     */
    public function setConsumerGender(?string $consumerGender): Job
    {
        $this->_consumerGender = $consumerGender;

        return $this;
    }

    /**
     * @return null|Address
     */
    public function getAddress(): ?Address
    {
        return $this->_address;
    }

    /**
     * @param null|Address $_address
     * @return self
     */
    public function setAddress(?Address $_address): self
    {
        $this->_address = $_address;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDate(): ?\DateTime
    {
        return $this->_date;
    }

    /**
     * @param \DateTime|null $_date
     * @return Job
     */
    public function setDate(?\DateTime $_date): Job
    {
        $this->_date = $_date;

        return $this;
    }

    /**
     * @return bool
     */
    public function isMorning(): bool
    {
        return $this->_morning;
    }

    /**
     * @param bool $_morning
     * @return self
     */
    public function setMorning(bool $_morning): self
    {
        $this->_morning = $_morning;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAfternoon(): bool
    {
        return $this->_afternoon;
    }

    /**
     * @param bool $_afternoon
     * @return self
     */
    public function setAfternoon(bool $_afternoon): self
    {
        $this->_afternoon = $_afternoon;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEvening(): bool
    {
        return $this->_evening;
    }

    /**
     * @param bool $_evening
     * @return self
     */
    public function setEvening(bool $_evening): self
    {
        $this->_evening = $_evening;

        return $this;
    }


    /**
     * @return Product
     */
    public function getPrimaryProduct(): Product
    {
        return $this->_primaryProduct;
    }

    /**
     * @param Product $_primaryProduct
     * @return self
     */
    public function setPrimaryProduct(Product $_primaryProduct): self
    {
        $this->_primaryProduct = $_primaryProduct;

        return $this;
    }

    /**
     * @return array
     */
    public function getSecondaryProducts(): array
    {
        return $this->_secondaryProducts;
    }

    /**
     * @param array $secondaryProducts
     * @return Job
     */
    public function setSecondaryProducts(array $secondaryProducts): Job
    {
        $this->_secondaryProducts = $secondaryProducts;

        return $this;
    }

    /**
     * @return string
     */
    public function getComments(): string
    {
        return $this->_comments;
    }

    /**
     * @param string $_comments
     * @return self
     */
    public function setComments(string $_comments): self
    {
        $this->_comments = $_comments;

        return $this;
    }

    /**
     * @return array
     */
    public function getAttachments(): array
    {
        return $this->_attachments;
    }

    /**
     * @param array $attachments
     */
    public function setAttachments(array $attachments): void
    {
        $this->_attachments = $attachments;
    }

    /**
     * @return bool|null
     */
    public function isBusinessJob(): ?bool
    {
        return $this->_businessJob;
    }

    /**
     * @param bool|null $businessJob
     * @return self
     */
    public function setBusinessJob(?bool $businessJob): self
    {
        $this->_businessJob = $businessJob;
        
        return $this;
    }

    /**
     * @return string|null
     */
    public function getVatNumber(): ?string
    {
        return $this->_vatNumber;
    }

    /**
     * @param string|null $vatNumber
     * @return self
     */
    public function setVatNumber(?string $vatNumber): self
    {
        $this->_vatNumber = $vatNumber;
        
        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        if ($this->_date === null) {
            throw new \LogicException('Date cannot be null');
        }
        if ($this->_primaryProduct === null) {
            throw new \LogicException('Primary product cannot be null');
        }
        if ($this->_businessJob === true && $this->_vatNumber === null) {
            throw new \LogicException('Vat number cannot be null for business job.');
        }

        $result = [
            'consumerName' => $this->_consumerName,
            'consumerEmail' => $this->_consumerEmail,
            'consumerPhoneNumber' => $this->_consumerPhoneNumber,
            'address' => $this->_address->jsonSerialize(),
            'date' => $this->_date->format(\DateTime::ISO8601),
            'morning' => $this->_morning,
            'afternoon' => $this->_afternoon,
            'evening' => $this->_evening,
            'primaryProduct' => $this->_primaryProduct->jsonSerialize(),
            'comments' => $this->_comments,
            'attachments' => $this->_attachments,
            'businessAssignment' => $this->_businessJob,
            'vatNumber' => $this->_vatNumber,
        ];

        if (count($this->_secondaryProducts) > 0) {
            $result['secondaryProducts'] = [];
            foreach ($this->_secondaryProducts as $code => $amount) {
                $result['secondaryProducts'][] = [
                    'product' => ['code' => ''.$code],
                    'amount' => (int) $amount,
                ];
            }
        }

        return $result;
    }
}
