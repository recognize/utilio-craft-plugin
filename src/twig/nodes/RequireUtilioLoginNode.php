<?php

namespace recognize\utilio\twig\nodes;

use recognize\utilio\Utilio;
use Twig_Compiler;

class RequireUtilioLoginNode extends \Twig_Node
{
    public function compile(Twig_Compiler $compiler)
    {
        $compiler
            ->addDebugInfo($this)
            ->write(Utilio::class . '::$plugin->loginService->requireLogin();' . PHP_EOL);
    }
}
