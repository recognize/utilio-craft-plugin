<?php

namespace recognize\utilio\twig;

use recognize\utilio\exceptions\UtilioException;
use recognize\utilio\models\Dealer;
use recognize\utilio\services\UtilioServiceInterface;
use recognize\utilio\twig\tokenparsers\RequireUtilioLoginTokenParser;
use recognize\utilio\Utilio;

/**
 * Class UtilioTwigExtension
 * @package recognize\utilio\twigextensions
 */
class UtilioTwigExtension extends \Twig_Extension
{
    /**
     * @return \Twig_SimpleFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new \Twig_SimpleFunction('productByCode', [$this, 'fetchProductByCode']),
            new \Twig_SimpleFunction('attachmentNameById', [$this, 'fetchAttachmentNameById']),
            new \Twig_SimpleFunction('utilioUser', [$this, 'currentUtilioUser']),
        ];
    }

    /**
     * @return array
     */
    public function getTokenParsers(): array
    {
        return [
            new RequireUtilioLoginTokenParser(),
        ];
    }

    /**
     * @param string|\Twig_Markup $code
     * @return array|null
     * @throws UtilioException
     */
    public function fetchProductByCode($code): ?array
    {
        $products = $this->getUtilioService()->getProducts(null, (string) $code);

        return $products['items'][0] ?? null;
    }

    /**
     * @return null|Dealer
     */
    public function currentUtilioUser(): ?Dealer
    {
        return Utilio::$plugin->loginService->getCurrentUtilioUser();
    }

    /**
     * @return string
     */
    public function fetchAttachmentNameById($attachmentId): string
    {
        $unknownAttachmentResponse = 'Onbekende bijlage';
        if (!is_numeric($attachmentId)) {
            return $unknownAttachmentResponse;
        }

        try {
            $attachment = Utilio::$plugin->utilioService->getAttachment((int)$attachmentId);

            return $attachment['originalName'] ?? $unknownAttachmentResponse;
        } catch (UtilioException $ex) {
            return $unknownAttachmentResponse;
        }
    }

    /**
     * @return UtilioServiceInterface
     */
    private function getUtilioService(): UtilioServiceInterface
    {
        return Utilio::$plugin->utilioService;
    }
}
