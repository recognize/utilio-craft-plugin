<?php

namespace recognize\utilio\twig\tokenparsers;

use recognize\utilio\twig\nodes\RequireUtilioLoginNode;

/**
 * Class RequireUtilioLoginTokenParser
 * @package recognize\utilio\twig\tokenparsers
 */
class RequireUtilioLoginTokenParser extends \Twig_TokenParser
{
    /**
     * @param \Twig_Token $token
     * @return RequireUtilioLoginNode|\Twig_Node
     * @throws \Twig_Error_Syntax
     */
    public function parse(\Twig_Token $token)
    {
        $line = $token->getLine();
        $this->parser->getStream()->expect(\Twig_Token::BLOCK_END_TYPE);

        return new RequireUtilioLoginNode([], [], $line, $this->getTag());
    }

    /**
     * @return string
     */
    public function getTag(): string
    {
        return 'requireUtilioLogin';
    }
}
