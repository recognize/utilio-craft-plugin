<?php
/**
 * Utilio plugin for Craft CMS 3.x
 *
 * Integrates
 *
 * @link      https://recognize.nl
 * @copyright Copyright (c) 2018 Recognize
 */

/**
 * Utilio en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('utilio', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    Recognize
 * @package   Utilio
 * @since     1.0.0
 */
return [
    'Utilio plugin loaded' => 'Utilio plugin loaded',
];
