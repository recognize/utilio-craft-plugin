<?php

namespace recognize\utilio\events;

use GuzzleHttp\ClientInterface;
use yii\base\Event;

/**
 * Class CreateClientEvent
 */
class CreateClientEvent extends Event
{
    /**
     * @var ClientInterface
     */
    public $client;
}
