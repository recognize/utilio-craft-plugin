<?php

namespace recognize\utilio\exceptions;

use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\Psr7\Response;
use Throwable;

/**
 * Class UtilioException
 */
class UtilioException extends \Exception
{
    /**
     * UtilioException constructor.
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return array|null
     */
    public function getErrorResponse(): ?array
    {
        $previous = $this->getPrevious();
        $errorResponse = null;

        if ($previous instanceof TransferException && ($response = $previous->getResponse()) instanceof Response) {
            /** @var Response $response */
            $errorResponse = json_decode((string) $response->getBody(), true);
        }

        if (is_array($errorResponse) && json_last_error() === JSON_ERROR_NONE) {
            return $errorResponse;
        }

        return null;
    }
}
