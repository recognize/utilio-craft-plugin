<?php

namespace recognize\utilio\controllers;

use craft\helpers\UrlHelper;
use craft\web\Controller;
use craft\web\Request;
use recognize\utilio\exceptions\UtilioException;
use recognize\utilio\popos\AvailabilityRequest;
use recognize\utilio\popos\ProductRequest;
use recognize\utilio\services\UtilioServiceInterface;
use recognize\utilio\Utilio;
use yii\base\Module;
use yii\web\Response;

/**
 *
 * @property UtilioServiceInterface $utilioService
 */
class ApiController extends Controller
{
    /**
     * ApiController constructor.
     * @param string $id
     * @param Module $module
     * @param array $config
     */
    public function __construct(string $id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->allowAnonymous = [
            'products' => self::ALLOW_ANONYMOUS_LIVE | self::ALLOW_ANONYMOUS_OFFLINE,
            'availability' => self::ALLOW_ANONYMOUS_LIVE | self::ALLOW_ANONYMOUS_OFFLINE,
            'attachment' => self::ALLOW_ANONYMOUS_LIVE | self::ALLOW_ANONYMOUS_OFFLINE,
            'delete' => self::ALLOW_ANONYMOUS_LIVE | self::ALLOW_ANONYMOUS_OFFLINE,
        ];
    }

    /**
     * @return Response
     * @throws UtilioException
     */
    public function actionProducts(): Response
    {
        /** @var Request $request */
        $request = \Craft::$app->request;
        $term = $request->getQueryParam('term', null) ?: null;
        $page = max(1, (int) $request->getQueryParam('page', 1));

        try {
            $service = $this->getUtilioService();
            $products = $service->getProducts($term, null, $page);
        } catch (UtilioException $ex) {
            \Craft::$app->response->statusCode = 400;

            return $this->asErrorJson($ex->getMessage());
        }

        return $this->asJson($products);
    }

    /**
     * @return Response
     */
    public function actionAvailability(): Response
    {
        /** @var Request $request */
        $request = \Craft::$app->request;
        $productId = $request->getQueryParam('product', null) ?: null;

        $from = (new \DateTime())->modify('+1 day');
        $until = (new \DateTime())->modify('+7 weeks +6 days');

        if ($productId === null) {
            \Craft::$app->response->statusCode = 400;

            return $this->asErrorJson('Product is missing');
        }

        $availabilityRequest = new AvailabilityRequest($from, $until);
        $availabilityRequest->addProduct(new ProductRequest($productId));

        try {
            $service = $this->getUtilioService();
            $availability = $service->getAvailability($availabilityRequest);
        } catch (UtilioException $ex) {
            \Craft::$app->response->statusCode = 400;

            return $this->asErrorJson($ex->getMessage());
        }

        return $this->asJson($availability);
    }

    public function actionAttachment()
    {
        /** @var Request $request */
        $request = \Craft::$app->request;
        $service = $this->getUtilioService();

        if (isset($_FILES['file']) && is_array($_FILES['file'])) {
            $file = $_FILES['file'];

            if ($file['error'] === UPLOAD_ERR_OK) {
                $handle = fopen($file['tmp_name'], 'r');

                try {
                    $attachmentId = $service->uploadAttachment($handle, $file['name']);

                    return $this->asJson([
                        'files' => [
                            [
                                'id' => $attachmentId,
                                'name' => $file['name'],
                                'size' => $file['size'],
                                'deleteUrl' => UrlHelper::url('utilio-api/attachment/'.$attachmentId),
                                'deleteType' => 'DELETE',
                            ],
                        ],
                    ]);
                } catch (UtilioException $ex) {
                    return $this->asJson([
                        'files' => [
                            [
                                'name' => $file['name'],
                                'size' => $file['size'],
                                'error' => $ex->getMessage(),
                            ],
                        ],
                    ]);
                }
            }
        }

        return $this->asJson([
            'files' => [
                [
                    'name' => $_FILES['file']['name'] ?? 'Unknown',
                    'size' => $_FILES['file']['size'] ?? 0,
                    'error' => 'Unexpected error',
                ],
            ],
        ]);
    }

    public function actionDelete(int $id)
    {
        $service = $this->getUtilioService();
        $service->deleteAttachment($id);
    }

    /**
     * @return UtilioServiceInterface
     */
    private function getUtilioService(): UtilioServiceInterface
    {
        return Utilio::$plugin->utilioService;
    }
}
