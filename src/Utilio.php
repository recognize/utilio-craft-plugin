<?php

namespace recognize\utilio;

use Craft;
use craft\base\Plugin;
use craft\events\RegisterUrlRulesEvent;
use craft\helpers\UrlHelper;
use craft\models\Site;
use craft\web\Controller;
use craft\web\UrlManager;
use craft\web\User;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use LogicException;
use recognize\utilio\models\Settings;
use recognize\utilio\models\SiteSettings;
use recognize\utilio\services\LoginService;
use recognize\utilio\services\UtilioService;
use recognize\utilio\services\UtilioServiceInterface;
use recognize\utilio\services\UtilioServiceStub;
use recognize\utilio\twig\UtilioTwigExtension;
use yii\base\Event;
use yii\web\Response;

/**
 * Craft plugins are very much like little applications in and of themselves. We’ve made
 * it as simple as we can, but the training wheels are off. A little prior knowledge is
 * going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, we’re going to assume that you know PHP and SQL,
 * as well as some semi-advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://craftcms.com/docs/plugins/introduction
 *
 * @author    Recognize
 * @package   Utilio
 * @since     1.0.0
 *
 * @property  UtilioServiceInterface $utilioService
 * @property  LoginService           $loginService
 * @property  ClientInterface        $client
 */
class Utilio extends Plugin
{
    /**
     * Static property that is an instance of this plugin class so that it can be accessed via
     * Utilio::$plugin
     *
     * @var Utilio
     */
    public static $plugin;

    public function __construct($id, $parent = null, array $config = [])
    {
        parent::__construct($id, $parent, $config);

        $this->schemaVersion = '1.0.0';
        $this->hasCpSettings = true;
    }

    /**
     * Set our $plugin static property to this class so that it can be accessed via
     * Utilio::$plugin
     *
     * Called after the plugin class is instantiated; do any one-time initialization
     * here such as hooks and events.
     *
     * If you have a '/vendor/autoload.php' file, it will be loaded for you automatically;
     * you do not need to load it in your init() method.
     *
     */
    public function init(): void
    {
        parent::init();
        self::$plugin = $this;

        $this->setComponents(
            [
                'utilioService' => UtilioService::class,
                'loginService'  => LoginService::class,
                'client'        => Client::class,
            ]
        );

        // Add in our Twig extensions
        Craft::$app->view->registerTwigExtension(new UtilioTwigExtension());

        // Register our site routes
        Event::on(
            UrlManager::class,
            UrlManager::EVENT_REGISTER_SITE_URL_RULES,
            function (RegisterUrlRulesEvent $event) {
                $event->rules['utilio-api/products'] = 'utilio/api/products';
                $event->rules['utilio-api/availability'] = 'utilio/api/availability';
                $event->rules['DELETE utilio-api/attachment/<id:\d+>'] = 'utilio/api/delete';
                $event->rules['POST utilio-api/attachment'] = 'utilio/api/attachment';
            }
        );
        if (($request = Craft::$app->request) && $request->getIsSiteRequest()) {
            Craft::$app->user->identity = $this->loginService->getCurrentUtilioUser();
            Event::on(
                User::class,
                User::EVENT_BEFORE_LOGOUT,
                function () {
                    $this->loginService->logoutAndRedirect();
                }
            );
        }

        if (CRAFT_ENVIRONMENT === 'test') {
            $this->setComponents(
                [
                    'utilioService' => UtilioServiceStub::class,
                ]
            );
        }

        Craft::info(
            Craft::t(
                'utilio',
                '{name} plugin loaded',
                ['name' => $this->name]
            ),
            __METHOD__
        );
    }

    /**
     * @param Site $site
     * @return SiteSettings
     */
    public static function getSiteSettingsOrFail(Site $site): SiteSettings
    {
        $pluginInstance = self::getInstance();
        if ($pluginInstance === null) {
            throw new LogicException('Expected Utilio plugin but was null');
        }

        $settings = $pluginInstance->getSettings();
        if ($settings === null) {
            throw new LogicException('Expected settings but was null');
        }

        if (!isset($settings->getMappedSiteSettings()[$site->handle])) {
            throw new \LogicException('Expected settings for site: '.$site->handle);
        }

        return $settings->getMappedSiteSettings()[$site->handle];
    }

    /**
     * @return Response
     */
    public function getSettingsResponse(): Response
    {
        $view = Craft::$app->getView();
        $namespace = $view->getNamespace();
        $view->setNamespace('settings');
        $view->setNamespace($namespace);

        /** @var Controller $controller */
        $controller = Craft::$app->controller;

        $tabs = [];
        $selectedTab = 0;
        foreach (Craft::$app->sites->getAllSites() as $index => $site) {
            $tabs[] = [
                'label' => $site->name,
                'url'   => UrlHelper::baseCpUrl().'admin/settings/plugins/utilio?site='.$site->handle,
            ];
            if (Craft::$app->request->getParam('site') === $site->handle) {
                $selectedTab = $index;
            }
        }

        return $controller->renderTemplate(
            'utilio/settings',
            [
                'plugin'       => $this,
                'tabs'         => $tabs,
                'site'         => Craft::$app->request->getParam('site', 'default'),
                'selectedTab'  => $selectedTab,
                'settings'     => $this->getSettings(),
                'fullPageForm' => true,
            ]
        );
    }

    public function getSettings()
    {
        /** @var Settings $settings */
        $settings = parent::getSettings();

        foreach (Craft::$app->sites->getAllSites() as $site) {
            if (!isset($settings->siteSettings[$site->handle])) {
                $settings->siteSettings[$site->handle] = new SiteSettings();
            }
        }

        return $settings;
    }

    protected function createSettingsModel(): Settings
    {
        $siteSettings = [];
        foreach (Craft::$app->sites->getAllSites() as $site) {
            $siteSettings[$site->handle] = new SiteSettings();
        }

        return new Settings(
            [
                'siteSettings' => $siteSettings,
            ]
        );
    }
}
