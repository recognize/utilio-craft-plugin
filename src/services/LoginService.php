<?php

namespace recognize\utilio\services;

use Craft;
use craft\base\Component;
use craft\helpers\UrlHelper;
use recognize\utilio\models\Dealer;
use yii\base\Exception;
use yii\base\ExitException;

/**
 * Class LoginService
 * @package recognize\utilio\services
 *
 * @property Dealer|null $currentUtilioUser
 * @property Dealer      $userAndRedirect
 * @property Dealer      $user
 */
class LoginService extends Component
{
    public $loginTarget = 'afspraak';

    /**
     * @throws ExitException
     * @throws Exception
     */
    public function requireLogin(): void
    {
        $user = Craft::$app->session->get('UtilioUser');

        if ($user === null) {
            $url = Craft::$app->user->loginUrl;
            $redirectUrl = Craft::$app->request->getAbsoluteUrl();
            Craft::$app->session->set('UtilioRedirect', $redirectUrl);

            Craft::$app->response->redirect(UrlHelper::siteUrl($url));
            Craft::$app->end();
        }
    }

    /**
     * @param Dealer $dealer
     * @throws ExitException
     * @throws Exception
     */
    public function setUserAndRedirect(Dealer $dealer): void
    {
        Craft::$app->session->set('UtilioUser', $dealer);

        Craft::$app->response->redirect(UrlHelper::siteUrl($this->loginTarget));
        Craft::$app->end();
    }

    /**
     * @throws ExitException
     * @throws Exception
     */
    public function logoutAndRedirect(): void
    {
        Craft::$app->session->remove('UtilioUser');
        Craft::$app->response->redirect(UrlHelper::siteUrl('/'));
        Craft::$app->end();
    }

    /**
     * @return null|Dealer
     */
    public function getCurrentUtilioUser(): ?Dealer
    {
        return Craft::$app->session->get('UtilioUser');
    }
}
