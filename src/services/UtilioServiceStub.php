<?php

namespace recognize\utilio\services;

use craft\base\Component;
use recognize\utilio\exceptions\UtilioException;
use recognize\utilio\popos\AvailabilityRequest;
use recognize\utilio\popos\Job;
use function count;

class UtilioServiceStub extends Component implements UtilioServiceInterface
{
    public const ITEMS = [
        [
            'code' => 'HY-1010',
            'description' => 'iets met glas',
            'name' => 'KPN Inhome Glas',
            'brand' => [
                'name' => 'KPN',
            ],
            'priceForCustomer' => 350,
            'outputPrice' => 0,
        ],
        [
            'code' => 'HY-1009',
            'description' => 'WIFI Inhome bevestigen',
            'name' => 'KPN WIFI inhome',
            'brand' => [
                'name' => 'KPN',
            ],
            'priceForCustomer' => 700,
            'outputPrice' => 0,
        ],
        [
            'code' => 'HY-1024',
            'description' => 'Demontage van KPN internet',
            'name' => 'KPN Internet-only Pakket',
            'brand' => [
                'name' => 'KPN',
            ],
            'priceForCustomer' => null,
            'outputPrice' => null,
        ],
        [
            'code' => 'HY-1023',
            'description' => 'Demonteren van T-Mobile',
            'name' => 'TELE2 Thuis Alles-in-1 Pakket',
            'brand' => [
                'name' => 'T-Mobile',
            ],
            'priceForCustomer' => null,
            'outputPrice' => null,
        ],
    ];

    /**
     * {@inheritdoc}
     */
    public function getProducts($term = null, $code = null, $page = 1): array
    {
        $items = self::ITEMS;

        if ($term !== null) {
            $items = array_filter($items, function ($item) use ($term) {
                return stripos($item['description'], $term) !== false;
            });
        }
        if ($code !== null) {
            $items = array_filter($items, function ($item) use ($code) {
                return strtolower($item['code']) === strtolower($code);
            });
        }

        if ($page > 1) {
            return [
                'currentPage' => $page,
                'itemsPerPage' => 50,
                'totalItems' => count($items),
                'items' => []
            ];
        }

        return [
            'currentPage' => $page,
            'itemsPerPage' => 50,
            'totalItems' => count($items),
            'items' => $items
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getProduct(string $code): array
    {
        foreach (self::ITEMS as $product) {
            if ($product['code'] === $code) {
                return $product;
            }
        }

        throw new UtilioException('Not found');
    }

    /**
     * {@inheritdoc}
     */
    public function getAvailability(AvailabilityRequest $availabilityRequest): array
    {
        $result = [];

        $current = clone $availabilityRequest->getFrom();
        while ($current <= $availabilityRequest->getUntil()) {
            $result[] = [
                'date' => $current->format('Y-m-d'),
                'morning' => true,
                'afternoon' => true,
                'evening' => true
            ];

            $current = $current->modify('+1 day');
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function postJob(Job $job): array
    {
        if ($job->getAddress() === null || $job->getAddress()->getPostcode() === '1234AA') {
            throw new UtilioException('Unknown error');
        }

        $json = $job->jsonSerialize();
        $json['address']['street'] = 'Akkerweg';
        $json['address']['city'] = 'Sibculo';

        $code = $json['primaryProduct']['code'];
        $matchingItems = array_values(array_filter(self::ITEMS, function ($item) use ($code) {
            return $item['code'] === $code;
        }));

        if (count($matchingItems) > 0) {
            $json['primaryProduct']['description'] = $matchingItems[0]['description'];
        }

        return $json;
    }

    /**
     * {@inheritdoc}
     */
    public function authenticate(string $username, string $password): array
    {
        if ($username === '12345' && $password === 'Test1234!') {
            return [
                'name' => 'Stein Dekker',
                'number' => '12345',
            ];
        }

        throw new UtilioException('Unable to authenticate');
    }

    /**
     * {@inheritDoc}
     */
    public function getAttachment(int $attachmentId): array
    {
        throw new UtilioException('Get attachment is not supported by stub');
    }

    /**
     * {@inheritDoc}
     */
    public function uploadAttachment($upload, $filename): int
    {
        throw new UtilioException('Upload attachment is not supported by stub');
    }

    /**
     * {@inheritDoc}
     */
    public function deleteAttachment($attachmentId): void
    {
        throw new UtilioException('Delete attachment is not supported by stub');
    }
}
