<?php

namespace recognize\utilio\services;

use GuzzleHttp\Client;
use recognize\utilio\exceptions\UtilioException;
use recognize\utilio\models\Settings;
use recognize\utilio\popos\AvailabilityRequest;
use recognize\utilio\popos\Job;

/**
 * UtilioService Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Recognize
 * @package   Utilio
 * @since     1.0.0
 *
 * @property Settings $settingsOrFail
 * @property Client $clientOrFail
 */
interface UtilioServiceInterface
{
    /**
     * @param string $code
     * @return array
     * @throws UtilioException
     */
    public function getProduct(string $code): array;

    /**
     * @param string|null $term
     * @param string|null $code
     * @param int $page
     * @return array
     * @throws UtilioException
     */
    public function getProducts($term = null, $code = null, $page = 1): array;

    /**
     * @param AvailabilityRequest $availabilityRequest
     * @return array
     * @throws UtilioException
     */
    public function getAvailability(AvailabilityRequest $availabilityRequest): array;

    /**
     * @param Job $job
     * @return array
     * @throws UtilioException
     */
    public function postJob(Job $job): array;

    /**
     * @param string $number
     * @param string $password
     * @return array
     * @throws UtilioException
     */
    public function authenticate(string $number, string $password): array;

    /**
     * @param int $attachmentId
     * @return array
     */
    public function getAttachment(int $attachmentId): array;

    /**
     * @param resource $upload
     * @param string $filename
     * @return int Attachment id
     */
    public function uploadAttachment($upload, $filename): int;

    /**
     * @param int $attachmentId
     */
    public function deleteAttachment($attachmentId): void;
}
