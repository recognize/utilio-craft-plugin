<?php

namespace recognize\utilio\services;

use Craft;
use craft\base\Component;
use craft\errors\SiteNotFoundException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\TransferException;
use GuzzleHttp\RequestOptions;
use recognize\utilio\events\CreateClientEvent;
use recognize\utilio\exceptions\UtilioException;
use recognize\utilio\models\Settings;
use recognize\utilio\models\SiteSettings;
use recognize\utilio\popos\AvailabilityRequest;
use recognize\utilio\popos\Job;
use recognize\utilio\Utilio;
use RuntimeException;
use function GuzzleHttp\Psr7\stream_for;

/**
 * UtilioService Service
 *
 * All of your plugin’s business logic should go in services, including saving data,
 * retrieving data, etc. They provide APIs that your controllers, template variables,
 * and other plugins can interact with.
 *
 * https://craftcms.com/docs/plugins/services
 *
 * @author    Recognize
 * @package   Utilio
 * @since     1.0.0
 *
 * @property Settings $settingsOrFail
 * @property Client   $clientOrFail
 */
class UtilioService extends Component implements UtilioServiceInterface
{
    public const EVENT_CREATE_CLIENT = 'createClient';

    /**
     * @var SiteSettings|null
     */
    public $siteSettings;

    /**
     * @var Client|null
     */
    private $_client;

    /**
     * UtilioService constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    /**
     * @throws SiteNotFoundException
     */
    public function init()
    {
        parent::init();

        if ($this->siteSettings === null) {
            $site = Craft::$app->sites->getCurrentSite();
            $settings = Utilio::getSiteSettingsOrFail($site);
            if ($settings->isNotSet()) {
                throw new RuntimeException("Site {$site->handle} is not yet configured in the Utilio Plugin.");
            }

            $this->siteSettings = $settings;
        }

        $client = new Client(
            ['base_uri' => $this->siteSettings->endpoint,
            'headers' => [
                'Authorization' => 'apikey ' . $this->siteSettings->apiKey,
                'Accept' => 'application/json',
            ],
        ]);
        $event = new CreateClientEvent();
        $event->client = $client;
        $this->trigger(self::EVENT_CREATE_CLIENT, $event);

        $this->_client = $event->client;
    }

    /**
     * {@inheritDoc}
     */
    public function getProduct(string $code): array
    {
        $client = $this->getClientOrFail();

        try {
            $response = $client->get('/client-api/v1/products/'.$code);

            return json_decode($response->getBody(), true);
        } catch (TransferException $ex) {
            Craft::error($ex);

            throw new UtilioException('Unable to fetch product', 0, $ex);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getProducts($term = null, $code = null, $page = 1): array
    {
        $client = $this->getClientOrFail();

        $query = ['page' => $page];
        if ($term !== null) {
            $query['term'] = $term;
        }
        if ($code !== null) {
            $query['code'] = $code;
        }

        try {
            $response = $client->get(
                '/client-api/v1/products',
                [
                    RequestOptions::QUERY => $query,
                ]
            );

            return json_decode($response->getBody(), true);
        } catch (TransferException $ex) {
            Craft::error($ex);

            throw new UtilioException('Unable to fetch products', 0, $ex);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getAvailability(AvailabilityRequest $availabilityRequest): array
    {
        $client = $this->getClientOrFail();

        try {
            $response = $client->post(
                '/client-api/v1/availability',
                [
                    RequestOptions::JSON => $availabilityRequest->jsonSerialize(),
                ]
            );

            return json_decode($response->getBody(), true) ?: [];
        } catch (TransferException $ex) {
            Craft::error($ex);

            throw new UtilioException('Unable to fetch availability', 0, $ex);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function postJob(Job $job): array
    {
        $client = $this->getClientOrFail();

        try {
            $response = $client->post(
                '/client-api/v1/jobs',
                [
                    RequestOptions::JSON => $job->jsonSerialize(),
                ]
            );

            return json_decode($response->getBody(), true) ?: [];
        } catch (TransferException $ex) {
            Craft::error($ex);

            throw new UtilioException('Unable to post job', 0, $ex);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function authenticate(string $number, string $password): array
    {
        $client = $this->getClientOrFail();

        try {
            $response = $client->post(
                '/client-api/v1/dealerlogin',
                [
                    RequestOptions::JSON => [
                        'number'   => $number,
                        'password' => $password,
                    ],
                ]
            );

            return json_decode($response->getBody(), true) ?: [];
        } catch (TransferException $ex) {
            Craft::error($ex);

            throw new UtilioException('Unable to authenticate', 0, $ex);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getAttachment(int $attachmentId): array
    {
        $client = $this->getClientOrFail();

        try {
            $response = $client->get('/client-api/v1/attachment/'.$attachmentId);

            return json_decode($response->getBody(), true) ?: [];
        } catch (TransferException $ex) {
            Craft::error($ex);

            throw new UtilioException('Unable to get attachment', 0, $ex);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function uploadAttachment($upload, $filename): int
    {
        $client = $this->getClientOrFail();

        try {
            $response = $client->post(
                '/client-api/v1/attachment',
                [
                    RequestOptions::MULTIPART => [
                        ['name' => 'file', 'contents' => stream_for($upload), 'filename' => $filename],
                    ],
                ]
            );

            return json_decode($response->getBody(), true)['id'] ?? -1;
        } catch (TransferException $ex) {
            Craft::error($ex);

            throw new UtilioException('Unable to upload file', 0, $ex);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function deleteAttachment($attachmentId): void
    {
        $client = $this->getClientOrFail();

        try {
            $client->delete('/client-api/v1/attachment/'.$attachmentId);
        } catch (TransferException $ex) {
            Craft::error($ex);

            throw new UtilioException('Unable to delete file', 0, $ex);
        }
    }

    /**
     * @return Client
     */
    private function getClientOrFail(): Client
    {
        $client = $this->_client;
        if ($client === null) {
            throw new \LogicException('Expected client but was null');
        }

        return $client;
    }
}
