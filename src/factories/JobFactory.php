<?php

namespace recognize\utilio\factories;

use recognize\utilio\popos\Product;
use recognize\utilio\popos\Address;
use recognize\utilio\popos\Job;

/**
 * Class JobFactory
 * @package luxaflex\factories
 */
class JobFactory
{
    public const DATE_FORMAT = 'Y-m-d';

    /**
     * @param array $jsonResponse
     * @return Job
     */
    public static function createFromJsonResponse(array $jsonResponse): Job
    {
        $job = new Job();

        $job
            ->setId($jsonResponse['id'] ?? null)
            ->setConsumerName($jsonResponse['consumerName'] ?? '')
            ->setConsumerEmail($jsonResponse['consumerEmail'] ?? '')
            ->setConsumerPhoneNumber($jsonResponse['consumerPhoneNumber' ?? ''])
            ->setAddress(self::createAddressFromJsonResponse($jsonResponse))
            ->setDate(self::parseDate($jsonResponse['date'] ?? ''))
            ->setMorning((bool) ($jsonResponse['morning'] ?? 0))
            ->setAfternoon((bool) ($jsonResponse['afternoon'] ?? 0))
            ->setEvening((bool) ($jsonResponse['evening'] ?? 0))
            ->setPrimaryProduct(self::createPrimaryProductFromJsonResponse($jsonResponse))
            ->setComments($jsonResponse['comments'] ?? '');

        return $job;
    }

    /**
     * @param array $jsonResponse
     * @return Address
     */
    private static function createAddressFromJsonResponse(array $jsonResponse): Address
    {
        $streetNumber = $jsonResponse['address']['streetNumber'] ?? '';
        $postcode = $jsonResponse['address']['postcode'] ?? '';

        $address = new Address($streetNumber, $postcode);
        $address
            ->setStreet($jsonResponse['address']['street'] ?? '')
            ->setCity($jsonResponse['address']['city'] ?? '');

        return $address;
    }

    /**
     * @param array $jsonResponse
     * @return Product
     */
    private static function createPrimaryProductFromJsonResponse(array $jsonResponse): Product
    {
        $code = $jsonResponse['primaryProduct']['code'] ?? '';
        $description = $jsonResponse['primaryProduct']['description'] ?? '';

        return (new Product($code))->setDescription($description);
    }

    /**
     * @param string $dateTime
     * @return \DateTime|null
     */
    private static function parseDate(string $dateTime): ?\DateTime
    {
        $matches = [];
        if (preg_match('/^(2\d{3}-[0-1]\d-[0-3]\d)/', $dateTime, $matches)) {
            return \DateTime::createFromFormat(self::DATE_FORMAT, $matches[1]);
        }

        return null;
    }
}
