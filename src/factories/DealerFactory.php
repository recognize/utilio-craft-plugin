<?php

namespace recognize\utilio\factories;

use recognize\utilio\models\Dealer;

/**
 * Class DealerFactory
 * @package recognize\utilio\factories
 */
class DealerFactory
{
    /**
     * @param array $json
     * @return Dealer
     */
    public static function createFromJsonResponse(array $json): Dealer
    {
        $dealer = new Dealer();
        $dealer->number = $json['number'] ?? '';
        $dealer->name = $json['name'] ?? '';

        return $dealer;
    }
}
