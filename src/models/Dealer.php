<?php

namespace recognize\utilio\models;

use craft\base\Model;
use yii\web\IdentityInterface;

/**
 * Class Login
 * @package luxaflex\models
 *
 * @property null|string $password
 * @property null        $authKey
 * @property null|string $id
 * @property bool        $admin
 */
class Dealer extends Model implements IdentityInterface
{
    /**
     * @var string|null
     */
    public $name;

    /**
     * @var string|null
     */
    public $number;

    /**
     * @var string|null
     */
    public $password;

    /**
     * @var bool
     */
    public $admin = false;

    /**
     * @return array
     */
    public function rules(): array
    {
        $requiredFields = ['number', 'password'];

        return [
            [$requiredFields, 'required'],
            [$requiredFields, 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public static function findIdentity($id)
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        return $this->number;
    }

    /**
     * {@inheritDoc}
     */
    public function getAuthKey()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function validateAuthKey($authKey)
    {
        return null;
    }
}
