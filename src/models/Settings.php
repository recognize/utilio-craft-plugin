<?php

namespace recognize\utilio\models;

use craft\base\Model;
use recognize\utilio\validators\SiteSettingsValidator;

/**
 * @package recognize\utilio\models
 */
class Settings extends Model
{
    /**
     * @var SiteSettings[]
     */
    public $siteSettings = [];

    /**
     * Workaround for getters and setters not working as expected.
     *
     * @return array
     */
    public function getMappedSiteSettings(): array
    {
        $mappedSiteSettings = [];
        foreach ($this->siteSettings as $site => $siteSetting) {
            $mappedSiteSettings[$site] = new SiteSettings($siteSetting);
        }

        return $mappedSiteSettings;
    }

    /**
     * This property is used in the SiteSettingsValidator to only validate the selected site.
     *
     * @var string
     */
    public $selectedSite = 'default';

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['siteSettings'], 'required'],
            ['siteSettings', SiteSettingsValidator::class],
        ];
    }
}
