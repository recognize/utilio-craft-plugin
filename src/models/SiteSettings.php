<?php

namespace recognize\utilio\models;

use craft\base\Model;
use recognize\utilio\validators\UtilioEndpointValidator;

/**
 * @package recognize\utilio\models
 */
class SiteSettings extends Model
{
    /**
     * @var string
     */
    public $endpoint = 'https://';

    /**
     * @var string
     */
    public $apiKey = '';

    /**
     * @return bool
     */
    public function isNotSet(): bool
    {
        return $this->endpoint === '' || $this->endpoint === 'https://' || $this->apiKey === '';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['apiKey', 'endpoint'], 'required'],
            ['endpoint', 'url'],
            ['endpoint', UtilioEndpointValidator::class]
        ];
    }
}
